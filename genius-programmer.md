# The One Secret Trick To Becoming A Genius Programmer

Okay, the title of this post is a bit of a lie. There's no *one* secret trick to becoming a genius programmer - there are *two*, and they're more habits than tricks. Nevertheless, these kind of 'secret tricks' seem to resonate with people, so I went for this title anyway.

Every once in a while, a somewhat strange thing happens to me. I'll be helping somebody out on IRC - usually a beginner - answering a number of their questions in rapid succession, about a variety of topics. Then after a while, they call me a "genius" for being able to answer everything they're asking; either directly, or while talking about me to somebody else.

Now, I don't really agree with this "genius" characterization, and it can make me feel a bit awkward, but it shows that a lot of developers have a somewhat idealistic and nebulous notion of the "genius programmer" - the programmer that knows everything, who can do everything, who's never stumped by a problem, and of which there are only a select few in the world; few enough that it's worth calling somebody a "genius" for it.

While this is an understandable notion from a beginner's point of view, the reality is that such genius programmers do not exist, or at least not in the way people usually think.

Now that's easy for me to say, given that I'm the one being called a 'genius', and you may well be looking up to some 'genius programmer' - whoever that might be - but feel like you'll never get there yourself. So, allow me to tell you precisely *how* to get to this seemingly all-knowing point, with just two simple habits.

I say 'seemingly', because I'm definitely not all-knowing, and neither is anybody else; my collected knowledge is just broad enough to make it *seem* that way to some others. Just like any other programmer, I make mistakes, I misunderstand things, and sometimes just don't understand things at all. Just like any other programmer, I sometimes get stumped on seemingly stupid problems, where it later turns out that I've spent a full day debugging a 2-character typo.

This is an important thing to remember; whoever your particular hero is, is (hopefully) a fallible human too; either that, or the alien invasion has already happened.

## The first habit: Research unfamiliar things

This is probably the one I see most people get wrong. They'll read a piece of text, run across an unfamiliar word or name, then decide that "oh, I can't understand this", and stop reading, ignoring the entirety of what they've read. Or even worse: ignoring the word itself, and reading the text as if it weren't there. If you do this, you will never advance; you will always be stuck with your existing set of knowledge.

Therefore, the first habit you need to learn is: __When you see an unfamiliar thing, read up on it right away, until you understand what it is.__

This doesn't mean that you need to learn about all the intricacies of *using* it, it just means that you need to learn its role in a project (if any), what its upsides and downsides are, and most of all, when and why it might be important for you to use it. Google it, read the Wikipedia page for it, just get a rough idea of what it is.

This is how I 'know' about so many things; I've run across them at some point in my life, and learned enough about them to place them in context and use them in answers. I may not have ever actually used it; I just know enough to point you in the right direction, and compare it to other options.

An extra benefit of this habit, is that people will be more willing to answer your questions on IRC or in other communities. If you show that you're willing to research the things you're being told about, then other people will be happy to give you more information and answer more complex questions.

## The second habit: Always question your skills

Two other very common mistakes I see programmers making, are to 1) treat code as "done" once it's written, and 2) assume that they already know all there is to know about a subject. The reality is that code is never done, and you are never done learning either, contrary to what 'traditional' schooling may have taught you.

Therefore, the second habit you need to learn is: __Always question your skills. Don't be afraid to go back and *change* code that you've already written, when you think you've discovered a better way to do it. Never assume that your knowledge of anything is perfect; always assume that you *might* be wrong, and actively look for cues that suggest you're missing some piece of knowledge.__

This is not to say that you should get stuck thinking about the perfect way to do something; if all you're doing is thinking and not writing code, you're never actually gaining any experience! It just means that it's always allowed - and encouraged! - to change and refine code, even if it's functionally already completed. In the same way, it will help immensely to ask other programmers to critique your code or approaches. *Improving existing code* is far more important and valuable than writing *new* code.

An extra benefit of this habit, is that you will reduce technical debt in projects a lot; if you're always refining your code to your best understanding at the time, there's no chance for it to become outdated or inconsistent.

This might sound like a lot of work at first, but you'll find that "improving code" is a skill like any other, and that you'll get orders of magnitude more efficient at doing it, as you practice with it more. Not just that, but it will also improve your *understanding* of code, which will help you write better code in the long run. In the end, it is absolutely worth it and will save you a lot of time.

## So... is that all?

Yes, and no. Really, these two habits aren't about programming *at all* - they're habits that will *help you learn how to learn*, in general. It just so happens that you're learning programming, and therefore those habits will help you advance with that. Becoming a good programmer involves much more than just these two habits; but these habits are a *critical* foundation for getting to the other things. If you get these two habits right, you will find that the rest follows by itself.

You don't have to trust me on my word, either. Give it a shot, and you'll see that it works!
